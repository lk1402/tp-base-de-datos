package main

import (
	"database/sql"
	_ "github.com/lib/pq"
	"log"
	"fmt"
	//"os"
	//"io/ioutil"
	bolt "github.com/boltdb/bolt"
	"time"
	"strconv"
	"encoding/json"
)

var existeBase bool
var existenTablas bool
var existenPKFK bool
var valoresInsertados bool
var sptrigCargados bool

type Cliente struct {
    Nrocliente int
    Nombre string
    Apellido string
    Domicilio string
    Telefono string
}

type Tarjeta struct {
    Nrotarjeta string
    Nrocliente int
    Validadesde string
    Validahasta string
    Codseguridad string
    Limitecompra float64
    Estado string
}

type Compra struct {
    Nrooperacion int//serial,
    Nrotarjeta string
    Nrocomercio int
    Fecha time.Time
    Monto float64
    Pagado bool
}

type Comercio struct {
    Nrocomercio int
    Nombre string
    Domicilio string
    Codigopostal string
    Telefono string
}

func main() {
	var letra string
	existeBase = false
	existenTablas = false
	existenPKFK = false
	valoresInsertados = false
	sptrigCargados = false

	go imprimir_menu()
	fmt.Scanf("%s", &letra)
	elegir_opcion(letra)
}

func elegir_opcion(opcion string){
	var letra2 string

	switch opcion {
		case "a":
			existeBase = opcion_a()
			existenTablas, existenPKFK, valoresInsertados = false, false, false
			fmt.Println("Salir o elegir otra opcion\n")
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "b":
			existenTablas = opcion_b()
			existenPKFK, valoresInsertados = false, false
			fmt.Println("Salir o elegir otra opcion\n")
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "c":
			if existenPKFK == false {
				existenPKFK = opcion_c()
				fmt.Println("Salir o elegir otra opcion\n")
			} else {
				fmt.Println("PKs y FKs ya asignadas, elija la opcion d para borrarlas si es eso lo que quiere hacer. Salir o elegir otra opcion\n")
			}
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "d":
			if (existenPKFK == true) {
				existenPKFK = opcion_d()
				fmt.Println("Salir o elegir otra opcion\n")
			} else {
				fmt.Println("PKs y FKs ya han sido borradas, ingrese opcion c para volver a autoasignarlas si es eso lo que quiere hacer. Salir o elegir otra opcion\n")
			}
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "e":
			if existeBase==true && existenTablas==true && existenPKFK==true && valoresInsertados==false {
				valoresInsertados = opcion_e()
				fmt.Println("Salir o elegir otra opcion\n")
			} else 	if(valoresInsertados == true) {
				fmt.Println("Valores por defecto ya ingreados. Salir o elegir otra opcion\n")
			} else {
				fmt.Println("Ejecute en secuencia las primeras tres opciones(a,b,c). Salir o elegir otra opcion\n")
			}
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "f":
			if sptrigCargados == false {
				sptrigCargados = opcion_f()
				fmt.Println("Salir o elegir otra opcion\n")
			} else {
				fmt.Println("SPs y Triggers ya estan cargados. Salir o elegir otra opcion\n")
			}
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "g":
			opcion_g()
			fmt.Println("Salir o elegir otra opcion\n")
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "h":
			opcion_h()      
			fmt.Println("Salir o elegir otra opcion\n")
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "i":
			opcion_i()      
			fmt.Println("Salir o elegir otra opcion\n")
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "j":
			opcion_j()      
			fmt.Println("Salir o elegir otra opcion\n")
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "k":
			opcion_k()      
			fmt.Println("Salir o elegir otra opcion\n")
			imprimir_menu()
			fmt.Scanf("%s", &letra2)
			elegir_opcion(letra2)
		case "x":
			// salir
		default:
		
		fmt.Println( "\n")
		fmt.Println("Tecla no valida, intente nuevamente\n")
		imprimir_menu()
		fmt.Scanf("%s", &letra2)
		elegir_opcion(letra2)
	}
}

func imprimir_menu(){

	fmt.Printf("a. Crear Base de Datos \n")
	fmt.Printf("b. Crear Tablas \n")
	fmt.Printf("c. Autoasignar PKs y FKs\n")
	fmt.Printf("d. Borrar PKs y FKs\n")
	fmt.Printf("e. Cargar Tablas \n")
	fmt.Printf("f. Cargar Stored Procedures y Triggers\n")
	fmt.Printf("g. Cargar Consumos de Prueba\n")
	fmt.Printf("h. Realizar Compra   \n")
	fmt.Printf("i. Generar Resumen \n")
	fmt.Printf("j. Crear Base de Datos No sql \n")
	fmt.Printf("k. Migrar a nosql \n")
	fmt.Printf("x. Salir \n")
}

func opcion_a() bool {

	fmt.Printf("Elegiste opcion a \n")
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer db.Close()
	
	_, err = db.Exec(`
		drop database if exists facturacion;
	`)

	if err != nil {
		log.Fatal(err)
		return false
	}

	_, err = db.Exec(`
		create database facturacion;
	`)

	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func opcion_b() bool {

	fmt.Printf("Elegiste opcion b \n")
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer db.Close()

	_, err = db.Exec(`
		begin;
		drop table if exists alerta;
		drop table if exists rechazo;
		drop table if exists cabecera;
		drop table if exists cierre;
		drop table if exists detalle;
		drop table if exists compra;
		drop table if exists consumo;
		drop table if exists comercio;
		drop table if exists tarjeta;
		drop table if exists cliente;
		
		create table cliente(nrocliente int, nombre text, apellido text, domicilio text, telefono char(12));
		create table tarjeta(nrotarjeta char(16), nrocliente int, validadesde char(6), validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10));
		create table comercio(nrocomercio int, nombre text, domicilio text, codigopostal char(8), telefono char(12));
		create table compra(nrooperacion serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean);
		create table rechazo(nrorechazo serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text);
		create table cierre(año int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date);
		create table cabecera(nroresumen serial, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date, vence date, total decimal(8,2));
		create table detalle(nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2));
		create table alerta(nroalerta serial, nrotarjeta char(16), fecha timestamp, nrorechazo int, codalerta int, descripcion text);
		create table consumo(nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2));
		commit;
	`)

	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func opcion_c() bool {
	fmt.Printf("Elegiste opcion c \n")
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer db.Close()

	_, err = db.Exec(`
		begin;
		alter table cliente add constraint cliente_pk primary key(nrocliente);
		alter table tarjeta add constraint tarjeta_pk primary key(nrotarjeta);
		alter table comercio add constraint comercio_pk primary key(nrocomercio);
		alter table compra add constraint compra_pk primary key(nrooperacion);
		alter table rechazo add constraint rechazo_pk primary key(nrorechazo);
		alter table cierre add constraint cierre_pk primary key(año, mes, terminacion);
		alter table cabecera add constraint cabecera_pk primary key(nroresumen);
		alter table detalle add constraint detalle_pk primary key(nroresumen, nrolinea);
		alter table alerta add constraint alerta_pk primary key(nroalerta);

		alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key(nrocliente) references cliente(nrocliente);
		alter table compra add constraint compra_nrotarjeta_fk foreign key(nrotarjeta) references tarjeta(nrotarjeta);
		alter table compra add constraint compra_nrocomercio_fk foreign key(nrocomercio) references comercio(nrocomercio);
		alter table rechazo add constraint rechazo_nrocomercio_fk foreign key(nrocomercio) references comercio(nrocomercio);
		alter table consumo add constraint consumo_nrocomercio_fk foreign key(nrocomercio) references comercio(nrocomercio);
		alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key(nrotarjeta) references tarjeta(nrotarjeta);
		alter table alerta add constraint alerta_nrorechazo_fk foreign key(nrorechazo) references rechazo(nrorechazo);
		commit;
	`)

	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func opcion_d() bool {
	fmt.Printf("Elegiste opcion d \n")
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`
		begin;
		alter table tarjeta drop constraint TARJETA_NROCLIENTE_FK;
		alter table compra drop constraint COMPRA_NROTARJETA_FK;
		alter table compra drop constraint COMPRA_NROCOMERCIO_FK;
		alter table rechazo drop constraint RECHAZO_NROCOMERCIO_FK;
		alter table consumo drop constraint CONSUMO_NROCOMERCIO_FK;
		alter table cabecera drop constraint CABECERA_NROTARJETA_FK;
		alter table alerta drop constraint ALERTA_NRORECHAZO_FK;

		alter table cliente drop constraint CLIENTE_PK;
		alter table tarjeta drop constraint TARJETA_PK;
		alter table comercio drop constraint COMERCIO_PK;
		alter table compra drop constraint COMPRA_PK;
		alter table rechazo drop constraint RECHAZO_PK;
		alter table cierre drop constraint CIERRE_PK;
		alter table cabecera drop constraint CABECERA_PK;
		alter table detalle drop constraint DETALLE_PK;
		alter table alerta drop constraint ALERTA_PK;
		commit;
	`)

	if err != nil {
		log.Fatal(err)
	}
	return false
}

func opcion_e() bool {
	fmt.Printf("Elegiste opcion e \n")
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer db.Close()

	_, err = db.Exec(`
		begin;
		insert into cliente values(001, 'Ulrich', 'Rothstein', 'Mar Del Plata, Rawson 1233', '345829005920');
		insert into cliente values(002, 'Brock', 'Scurry', 'Rosario, Saavedra 1175', '210881326678');
		insert into cliente values(003, 'Isabela', 'Barbosa', 'Flores, Miraflores 2148', '863877377049');
		insert into cliente values(004, 'Asmart', 'Karataev', 'Tandil, Santamarina 36', '751351815265');
		insert into cliente values(005, 'Ladislav', 'Altmann', 'Chivilcoy, Soárez 80 ', '545858690461');
		insert into cliente values(006, 'Jiao', 'Sung', 'Villa Crespo, Gurruchaga 469', '280415512921');
		insert into cliente values(007, 'Marko', 'Stanković', 'General Alvear, Yrigoyen 630', '302859658527');
		insert into cliente values(008, 'Asger', 'Mortensen', 'Mar Del Plata, Garay 3127', '846808377038');
		insert into cliente values(009, 'Gebre', 'Dawit', 'Córdoba, Armenia 1847', '246659621496');
		insert into cliente values(010, 'Kieran', 'Taylor', 'Junín, Belgrano 127', '283386968818');
		insert into cliente values(011, 'Qazim', 'Balavac', 'Almagro, Troilo 990', '326428890696');
		insert into cliente values(012, 'Debbie', 'Tourville', 'Bahía Blanca, Alsina 35', '328018411164');
		insert into cliente values(013, 'Carola', 'Heiskanen', 'Llavallol, Pronzato 323', '226383248549');
		insert into cliente values(014, 'Alexis', 'Racine', 'General Villegas, Pringles 351', '269082312943');
		insert into cliente values(015, 'Edvard', 'Broberg', 'Azul, Yrigoyen 424', '219427653321');
		insert into cliente values(016, 'Lavinia', 'Palerma', 'Bahía Blanca, Zelarrayán 2143', '442343251214');
		insert into cliente values(017, 'Mariya', 'Takeuchi', 'Olavarría, Rivadavia 2691', '263093962048');
		insert into cliente values(018, 'Zara', 'Avdeyeva', 'Posadas, Uruguay 3102', '534015661364');
		insert into cliente values(019, 'Elias', 'Bergqvist', 'Recoleta, Laprida 918', '228691405367');
		insert into cliente values(020, 'Ghawth', 'Mustafa', 'San Nicolás, Primavera 630', '269586667104');

		insert into comercio values(001, 'Nunc In At Institute', 'Buenos Aires, Avenida Cabildo 2230', '91744849', '325354917953');
		insert into comercio values(002, 'Sit Incorporated', 'Cipolletti, Muñoz 391', '91744849', '780366742925');
		insert into comercio values(003, 'Suspendisse Limited', 'Pilar, Tucumán 162', '91744849', '341879568923');
		insert into comercio values(004, 'In Ltd', 'Balvanera, Moreno 1785', '91744849', '829456299434');
		insert into comercio values(005, 'Luctus Ipsum Incorporated', 'Caseros, San Martín 2130', '91744849', '832462984236');
		insert into comercio values(006, 'Nunc Risus Varius PC', 'Balvanera, Gómez 2813', '91744849', '780759523204');
		insert into comercio values(007, 'Ullamcorper Viverra Maecenas LLP', 'Caballito, Moreno 788', '91744849', '299114718014');
		insert into comercio values(008, 'Mollis Industries', 'Villa Devoto, Baigorria 4540', '91744849', '457609308316');
		insert into comercio values(009, 'Iaculis Quis Industries', 'Córdoba, Entre Ríos 233', '91744849', '274147442492');
		insert into comercio values(010, 'Nullam Ut Nisi Institute', 'Moreno, Pirovano 2960', '91744849', '287201324076');
		insert into comercio values(011, 'Orci PC', 'Resistencia, San Martín 83', '90148431', '105565838602');
		insert into comercio values(012, 'Fringilla Mi Corp', 'Santa Rosa, Palacios 1545', '42365434', '631763336745');
		insert into comercio values(013, 'Rhoncus Limited', 'La Plata, Avenida 44 650', '27066730', '601906009202');
		insert into comercio values(014, 'Nullam Velit Limited', 'Villa Mercedes, Córdoba 88', '98525162', '759295903863');
		insert into comercio values(015, 'Magna Suspendisse Institute', 'Santa Fé, Pellegrini 3390', '55733387', '267320864732');
		insert into comercio values(016, 'Pellentesque Limited', 'Villa Lynch, Peña 4576', '17970979', '970796357867');
		insert into comercio values(017, 'Sem LLP', 'Tandil, 14 de Julio 1562', '91358779', '628916225407');
		insert into comercio values(018, 'Ligula Tortor Inc', 'Bahía Blanca, Zapiola 913', '71002132', '549833878169');
		insert into comercio values(019, 'Ac Eleifend Foundation', 'Ciudadela, Falcón 3674', '75246018', '723117747633');
		insert into comercio values(020, 'Praesent Luctus Curabitur Company', 'Morón, Balbín 966', '67067168', '161542586699');

		insert into tarjeta values('9746521427739620', 001, '201711', '202310', '9876', 99652.44, 'vigente');
		insert into tarjeta values('9432508188357224', 002, '201704', '202309', '2947', 79751.79, 'vigente');
		insert into tarjeta values('3608343552173256', 003, '201707', '202304', '5895', 70133.59, 'suspendida');
		insert into tarjeta values('2507045591093716', 004, '201711', '202302', '2224', 78057.27, 'vigente');
		insert into tarjeta values('3131072254223251', 005, '201712', '202305', '7475', 87135.08, 'suspendida');
		insert into tarjeta values('6266032642696601', 006, '201707', '201807', '9574', 42965.66, 'anulada');
		insert into tarjeta values('3676128131588359', 007, '201706', '201811', '5087', 43850.39, 'anulada');
		insert into tarjeta values('5733599117502494', 008, '201704', '201808', '6332', 59726.75, 'anulada');
		insert into tarjeta values('6900769832851312', 009, '201705', '201806', '3542', 22058.74, 'anulada');
		insert into tarjeta values('7719151705855599', 010, '201703', '201808', '3974', 81343.95, 'anulada');
		insert into tarjeta values('4655147192654723', 011, '201710', '202301', '6673', 98562.14, 'vigente');
		insert into tarjeta values('8410183192865833', 012, '201709', '202310', '6199', 49108.03, 'vigente');
		insert into tarjeta values('3657212838127535', 013, '201705', '202309', '6434', 29335.36, 'vigente');
		insert into tarjeta values('3894129187263347', 013, '201706', '202302', '3421', 80421.70, 'vigente');
		insert into tarjeta values('4957579864892580', 014, '201701', '202307', '4189', 59087.72, 'vigente');
		insert into tarjeta values('6720272101278090', 015, '201703', '202304', '5954', 62325.77, 'vigente');
		insert into tarjeta values('4586534544794263', 016, '201705', '202310', '3004', 86453.84, 'vigente');
		insert into tarjeta values('3957616251384709', 016, '201710', '202306', '9056', 86613.77, 'vigente');
		insert into tarjeta values('8595462893090711', 017, '201705', '202304', '3587', 12039.15, 'suspendida');
		insert into tarjeta values('7456360239739667', 018, '201701', '202301', '1279', 75821.84, 'vigente');
		insert into tarjeta values('6122401822518442', 019, '201705', '202305', '5855', 96353.96, 'vigente');
		insert into tarjeta values('1453090328582637', 020, '201704', '202306', '8516', 87267.51, 'suspendida');

		insert into cierre values(2019,1,0,'2018-12-20','2019-01-19','2019-01-29'), (2019,1,1,'2018-12-21','2019-01-20','2019-01-30'),
		(2019,1,2,'2018-12-22','2019-01-21','2019-01-31'),	(2019,1,3,'2018-12-23','2019-01-22','2019-02-02'),
		(2019,1,4,'2018-12-24','2019-01-23','2019-02-03'),  (2019,1,5,'2018-12-25','2019-01-24','2019-02-04'),
		(2019,1,6,'2018-12-26','2019-01-25','2019-02-05'),	(2019,1,7,'2018-12-27','2019-01-28','2019-02-06'),
		(2019,1,8,'2018-12-28','2019-01-27','2019-02-07'),	(2019,1,9,'2018-12-29','2019-01-28','2019-02-07'),
		(2019,2,0,'2019-01-20','2019-02-19','2019-03-01'),	(2019,2,1,'2019-01-21','2019-02-20','2019-03-02'), 
		(2019,2,2,'2019-01-22','2019-02-21','2019-03-03'),	(2019,2,3,'2019-01-23','2019-02-22','2019-03-04'),
		(2019,2,4,'2019-01-24','2019-02-23','2019-03-05'),	(2019,2,5,'2019-01-25','2019-02-24','2019-03-06'),
		(2019,2,6,'2019-01-26','2019-02-25','2019-03-07'),	(2019,2,7,'2019-01-27','2019-02-26','2019-03-08'),
		(2019,2,8,'2019-01-28','2019-02-27','2019-03-09'),	(2019,2,9,'2019-01-29','2019-02-28','2019-03-10'),
		(2019,3,0,'2019-02-20','2019-03-19','2019-03-29'),	(2019,3,1,'2019-02-21','2019-03-20','2019-03-30'),
		(2019,3,2,'2019-02-22','2019-03-21','2019-03-31'),	(2019,3,3,'2019-02-23','2019-03-22','2019-04-01'),
		(2019,3,4,'2019-02-24','2019-03-23','2019-04-02'),	(2019,3,5,'2019-02-25','2019-03-25','2019-04-03'),
		(2019,3,6,'2019-02-26','2019-03-25','2019-04-04'),	(2019,3,7,'2019-02-27','2019-03-26','2019-04-05'),
		(2019,3,8,'2019-02-28','2019-03-27','2019-04-06'),	(2019,3,9,'2019-03-01','2019-03-28','2019-04-07'),
		(2019,4,0,'2019-03-20','2019-04-19','2019-04-29'),	(2019,4,1,'2019-03-21','2019-04-20','2019-04-30'),
		(2019,4,2,'2019-03-22','2019-04-21','2019-05-01'),	(2019,4,3,'2019-03-23','2019-04-22','2019-05-02'),
		(2019,4,4,'2019-03-24','2019-04-23','2019-05-03'),	(2019,4,5,'2019-03-25','2019-04-24','2019-05-04'),
		(2019,4,6,'2019-03-26','2019-04-25','2019-05-05'),	(2019,4,7,'2019-03-27','2019-04-26','2019-05-06'),
		(2019,4,8,'2019-03-28','2019-04-27','2019-05-07'),	(2019,4,9,'2019-03-29','2019-04-28','2019-05-08'),
		(2019,5,0,'2019-04-20','2019-05-19','2019-05-29'),	(2019,5,1,'2019-04-21','2019-05-20','2019-05-30'),
		(2019,5,2,'2019-04-22','2019-05-21','2019-05-31'),	(2019,5,3,'2019-04-23','2019-05-22','2019-06-01'),
		(2019,5,4,'2019-04-24','2019-05-23','2019-06-02'),	(2019,5,5,'2019-04-25','2019-05-24','2019-06-03'),
		(2019,5,6,'2019-04-26','2019-05-25','2019-06-04'),	(2019,5,7,'2019-04-27','2019-05-26','2019-06-05'),
		(2019,5,8,'2019-04-28','2019-05-27','2019-06-06'),	(2019,5,9,'2019-04-29','2019-05-28','2019-06-07'),
		(2019,6,0,'2019-05-20','2019-06-19','2019-06-29'),	(2019,6,1,'2019-05-21','2019-06-20','2019-06-30'),
		(2019,6,2,'2019-05-22','2019-06-21','2019-07-01'),	(2019,6,3,'2019-05-23','2019-06-22','2019-07-02'),
		(2019,6,4,'2019-05-24','2019-06-23','2019-07-03'),	(2019,6,5,'2019-05-25','2019-06-24','2019-07-04'),
		(2019,6,6,'2019-05-26','2019-06-25','2019-07-05'),	(2019,6,7,'2019-05-27','2019-06-26','2019-07-06'),
		(2019,6,8,'2019-05-28','2019-06-27','2019-07-07'),	(2019,6,9,'2019-05-29','2019-06-28','2019-07-08'),
		(2019,7,0,'2019-06-20','2019-07-19','2019-07-29'),	(2019,7,1,'2019-06-21','2019-07-20','2019-07-30'),
		(2019,7,2,'2019-06-22','2019-07-21','2019-07-31'),	(2019,7,3,'2019-06-23','2019-07-22','2019-08-01'),
		(2019,7,4,'2019-06-24','2019-07-23','2019-08-02'),	(2019,7,5,'2019-06-25','2019-07-24','2019-08-03'),
		(2019,7,6,'2019-06-26','2019-07-25','2019-08-04'),	(2019,7,7,'2019-06-27','2019-07-26','2019-08-05'),
		(2019,7,8,'2019-06-28','2019-07-27','2019-08-06'),	(2019,7,9,'2019-06-29','2019-07-28','2019-08-07'),
		(2019,8,0,'2019-07-20','2019-08-19','2019-08-29'),	(2019,8,1,'2019-07-21','2019-08-20','2019-08-30'),
		(2019,8,2,'2019-07-22','2019-08-21','2019-08-31'),	(2019,8,3,'2019-07-23','2019-08-22','2019-09-01'),
		(2019,8,4,'2019-07-24','2019-08-23','2019-09-02'),	(2019,8,5,'2019-07-25','2019-08-24','2019-09-03'),
		(2019,8,6,'2019-07-26','2019-08-25','2019-09-04'),	(2019,8,7,'2019-07-27','2019-08-26','2019-09-05'),
		(2019,8,8,'2019-07-28','2019-08-27','2019-09-06'),	(2019,8,9,'2019-07-29','2019-08-28','2019-09-07'),
		(2019,9,0,'2019-08-20','2019-09-19','2019-09-29'),	(2019,9,1,'2019-08-21','2019-09-20','2019-09-30'),
		(2019,9,2,'2019-08-22','2019-09-21','2019-10-01'),	(2019,9,3,'2019-08-23','2019-09-22','2019-10-02'),
		(2019,9,4,'2019-08-24','2019-09-23','2019-10-03'),	(2019,9,5,'2019-08-25','2019-09-24','2019-10-04'),
		(2019,9,6,'2019-08-26','2019-09-25','2019-10-05'),	(2019,9,7,'2019-08-27','2019-09-26','2019-10-06'),
		(2019,9,8,'2019-08-28','2019-09-27','2019-10-07'),	(2019,9,9,'2019-08-29','2019-09-28','2019-10-08'),
		(2019,10,0,'2019-09-20','2019-10-19','2019-10-29'),	(2019,10,1,'2019-09-21','2019-10-20','2019-10-30'),
		(2019,10,2,'2019-09-22','2019-10-21','2019-10-31'),	(2019,10,3,'2019-09-23','2019-10-22','2019-11-01'),
		(2019,10,4,'2019-09-24','2019-10-23','2019-11-02'),	(2019,10,5,'2019-09-25','2019-10-24','2019-11-03'),
		(2019,10,6,'2019-09-26','2019-10-25','2019-11-04'),	(2019,10,7,'2019-09-27','2019-10-26','2019-11-05'),
		(2019,10,8,'2019-09-28','2019-10-27','2019-11-06'),	(2019,10,9,'2019-09-29','2019-10-28','2019-11-07'),
		(2019,11,0,'2019-10-20','2019-11-19','2019-11-29'),	(2019,11,1,'2019-10-21','2019-11-20','2019-11-30'),
		(2019,11,2,'2019-10-22','2019-11-21','2019-12-01'),	(2019,11,3,'2019-10-23','2019-11-22','2019-12-02'),
		(2019,11,4,'2019-10-24','2019-11-23','2019-12-03'),	(2019,11,5,'2019-10-25','2019-11-24','2019-12-04'),
		(2019,11,6,'2019-10-26','2019-11-25','2019-12-05'),	(2019,11,7,'2019-10-27','2019-11-26','2019-12-06'),
		(2019,11,8,'2019-10-28','2019-11-27','2019-12-07'),	(2019,11,9,'2019-10-29','2019-11-28','2019-12-08'),
		(2019,12,0,'2019-11-20','2019-12-19','2019-12-29'),	(2019,12,1,'2019-11-21','2019-12-20','2019-12-30'),
		(2019,12,2,'2019-11-22','2019-12-21','2019-12-31'),	(2019,12,3,'2019-11-23','2019-12-22','2020-01-01'),
		(2019,12,4,'2019-11-24','2019-12-23','2020-01-02'),	(2019,12,5,'2019-11-25','2019-12-24','2020-01-03'),
		(2019,12,6,'2019-11-26','2019-12-25','2020-01-04'),	(2019,12,7,'2019-11-27','2019-12-26','2020-01-05'),
		(2019,12,8,'2019-11-28','2019-12-27','2020-01-06'),	(2019,12,9,'2019-11-29','2019-12-28','2020-01-07');
		commit;
	`)

	if err != nil {
		log.Fatal(err)
		return false
	}

	return true
}

func opcion_f() bool {
	fmt.Printf("Elegiste opcion f \n")
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`

		--AUTORIZACION DE COMPRA
		create or replace function autorizacioncompra(_nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2)) returns boolean as $$
		declare
			autorizado boolean := false;
			_estado tarjeta.estado%TYPE;
			_codseguridad tarjeta.codseguridad%TYPE;
			_montototal tarjeta.limitecompra%TYPE;
			_limitecompra tarjeta.limitecompra%TYPE;
			fecha char(6) := to_char(current_timestamp,'YYYYMM');
			_validahasta tarjeta.validahasta%TYPE;
			_motivo rechazo.motivo%TYPE;
		begin
			select get_monto_adeudado(_nrotarjeta) into _montototal;
			_montototal := _montototal + monto;
			select t.estado, t.codseguridad, t.limitecompra, t.validahasta into _estado, _codseguridad, _limitecompra, _validahasta from tarjeta t where t.nrotarjeta = _nrotarjeta;
			if(not found or _estado =  'anulada') then
				_motivo := '?tarjeta no válida ó no vigente';
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) values(_nrotarjeta, nrocomercio, current_timestamp, monto, _motivo);
			elsif (_codseguridad <> codseguridad) then
				_motivo := '?código de seguridad inválido';
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) values(_nrotarjeta, nrocomercio, current_timestamp, monto, _motivo);
			elsif (_estado = 'suspendida') then
				_motivo := '?la tarjeta se encuentra suspendida';
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) values(_nrotarjeta, nrocomercio, current_timestamp, monto, _motivo);
			elsif (_montototal > _limitecompra) then
				_motivo := '?supera límite de tarjeta';
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) values(_nrotarjeta, nrocomercio, current_timestamp, monto, _motivo);
			elsif (fecha > _validahasta) then
				_motivo := '?plazo de vigencia expirado';
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) values(_nrotarjeta, nrocomercio, current_timestamp, monto, _motivo);
			else
				autorizado := true;
			    insert into compra(nrotarjeta, nrocomercio, fecha, monto, pagado) values(_nrotarjeta, nrocomercio, current_timestamp, monto, false);
			end if;
			return autorizado;
		end;
		$$ language plpgsql;

		create function get_monto_adeudado(tarjeta char(16)) returns decimal(8,2) as $$
			declare
				suma decimal(8,2);
			begin
				select coalesce (sum(monto), 0.00) into suma from compra c
				where(c.nrotarjeta = tarjeta);
				return suma;
			end;
		$$ language plpgsql;

		--GENERACION DE RESUMEN
		create or replace function generarResumen(cliente int, _año int, _mes int) returns void as $$
		begin
			insert into cabecera(nombre, apellido, domicilio, nrotarjeta, desde, hasta, vence, total)
				select distinct c.nombre, c.apellido, c.domicilio, t.nrotarjeta, c1.fechainicio, c1.fechacierre, c1.fechavto, sum(c2.monto) from 
				cliente c inner join tarjeta t on c.nrocliente = t.nrocliente
				inner join cierre c1 on (to_number(right(t.nrotarjeta,1),'9') = c1.terminacion and c1.año = _año and c1.mes = _mes) 
				inner join compra c2 on t.nrotarjeta = c2.nrotarjeta
				where c.nrocliente = cliente and (to_date(c2.fecha::text, 'YYYY-MM-DD') between c1.fechainicio and c1.fechacierre)
				group by (c.nrocliente, c.nombre, c.apellido, c.domicilio, t.nrotarjeta, c1.fechainicio, c1.fechacierre, c1.fechavto);
		end;
		$$ language plpgsql;

		create or replace function generardetalle() returns trigger as $$
		begin
			insert into detalle
			select new.nroresumen, row_number() over(), to_date(c.fecha::text, 'YYYY-MM-DD'), c1.nombre, c.monto from
			tarjeta t inner join compra c on t.nrotarjeta = c.nrotarjeta
			inner join comercio c1 on c.nrocomercio = c1.nrocomercio
			where (t.nrotarjeta = new.nrotarjeta) and (to_date(c.fecha::text, 'YYYY-MM-DD') between new.desde and new.hasta);
			return new;
		end;
		$$ language plpgsql;

		create trigger generardetalletrg
		after insert on cabecera
		for each row
		execute procedure generardetalle();

		--ALERTAR CLIENTES
		create or replace function alerta_rechazo() returns trigger as $$
		declare
			codigo_alerta_rechazo alerta.codalerta%TYPE := 0;
		begin
			insert into alerta(nrotarjeta, fecha, nrorechazo, codalerta, descripcion) values(new.nrotarjeta, new.fecha, new.nrorechazo, codigo_alerta_rechazo, new.motivo);
			return new;
		end;
		$$ language plpgsql;

		create trigger rechazo
		after insert on rechazo
		for each row
		execute procedure alerta_rechazo();

		create or replace function alerta_compra_1() returns trigger as $$
		declare
			codigo_alerta_compra_1 alerta.codalerta%TYPE := 1;
			compra_encontrada record;
			_mensaje alerta.descripcion%TYPE := 'compra en comercio distinto de mismo codigo postal hace menos de 1 minuto';
		begin
			select into compra_encontrada from compra where
				compra.nrooperacion <> new.nrooperacion and
				compra.nrotarjeta = new.nrotarjeta and
				compra.fecha::date = new.fecha::date and
				compra.fecha::time > (new.fecha::time - '1 minute'::interval) and
				compra.nrocomercio <> new.nrocomercio and
				(select comercio.codigopostal from comercio where comercio.nrocomercio = compra.nrocomercio) = (select comercio.codigopostal from comercio where comercio.nrocomercio = new.nrocomercio);
			if found then
				insert into alerta(nrotarjeta, fecha, codalerta, descripcion) values(new.nrotarjeta, new.fecha, codigo_alerta_compra_1, _mensaje);
			end if;
			return new;
		end;
		$$ language plpgsql;

		create trigger compra_1
		before insert on compra
		for each row
		execute procedure alerta_compra_1();

		create or replace function alerta_compra_5() returns trigger as $$
		declare
			codigo_alerta_compra_5 alerta.codalerta%TYPE := 5;
			compra_encontrada record;
			_mensaje alerta.descripcion%TYPE := 'compra en comercio distinto de distinto codigo postal hace menos de 5 minutos'; 
		begin
			select into compra_encontrada from compra where
				compra.nrooperacion <> new.nrooperacion and
				compra.nrotarjeta = new.nrotarjeta and
				compra.fecha::date = new.fecha::date and
				compra.fecha::time > (new.fecha::time - '5 minute'::interval) and
				(select comercio.codigopostal from comercio where comercio.nrocomercio = compra.nrocomercio) <> (select comercio.codigopostal from comercio where comercio.nrocomercio = new.nrocomercio);
			if found then
				insert into alerta(nrotarjeta, fecha, codalerta, descripcion) values(new.nrotarjeta, new.fecha, codigo_alerta_compra_5, _mensaje);
			end if;
			return new;
		end;
		$$ language plpgsql;

		create trigger compra_5
		before insert on compra
		for each row
		execute procedure alerta_compra_5();

		create or replace function alerta_limite() returns trigger as $$
		declare
			codigo_alerta_limite alerta.codalerta%TYPE := 32;
			limite rechazo.motivo%TYPE := '?supera límite de tarjeta';
			suspension alerta.descripcion%TYPE := 'doble alerta de limite en el mismo día, se suspenderá la tarjeta';
		begin
			if(select count(*) from rechazo r 
			where (r.nrotarjeta = new.nrotarjeta and r.motivo = limite and to_date(r.fecha::text, 'YYYY-MM-DD') = to_date(new.fecha::text, 'YYYY-MM-DD'))) > 1 then
				update alerta set codalerta = codigo_alerta_limite, descripcion = suspension where nroalerta = new.nroalerta;
				update tarjeta set estado = 'suspendida' where tarjeta.nrotarjeta = new.nrotarjeta;
			end if; 
			return new;
		end;
		$$ language plpgsql;

		create trigger limite
		after insert on alerta
		for each row
		when(new.descripcion = '?supera límite de tarjeta')
		execute procedure alerta_limite();
	
		create or replace function ejecutar_consumos_virtuales() returns trigger as $$
			begin
				perform  autorizacioncompra(new.nrotarjeta,new.codseguridad, new.nrocomercio, new.monto);
				return new;
			end;
		$$ language plpgsql;

		create trigger consumo_virtual
		after insert on consumo
		for each row
		execute procedure ejecutar_consumos_virtuales()
	`)

	if err != nil {
		log.Fatal(err)
	}
	return true
}


func opcion_g() bool{

	fmt.Printf("Elegiste opcion g \n")
	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
		return false
	}
	defer db.Close()

	_, err = db.Exec(`
		begin;
		insert into consumo values ('9746521427739620','9877',4,99652.44),
			('9746521427739620','9876',4,99652.44),
			('9432508188357225','2947',4,300.44),
			('3131072254223251','7475',4,20000.00),
			('5733599117502494','6332',4,25.44),
			('4957579864892580','4189',4,59087.73),
			('9746521427739620','9876',4,0.01),
			('9746521427739620','9876',4,10.01),
			('3657212838127535','6434',4,2001.00),
			('3657212838127535','6434',7,2004.00),
			('3894129187263347','3421',16,2002.00),
			('3894129187263347','3421',12,2016.00),
			('6900769832851312','3542',5,30000.00),
			('6900769832851312','3542',5,30000.00),
			('4586534544794263','3004',9,8001.00),
			('4586534544794263','3004',9,8002.00),
			('3957616251384709','9056',9,8004.00),
			('3957616251384709','9056',9,8008.00);
		commit;
		`)
	if err != nil {
		log.Fatal(err)
		return false
	}
	return true
}

func opcion_h() {
	fmt.Printf("Elegiste opcion h \n")

	var _nrotarjeta string
	var codseguridad string
	var nrocomercio string
	var monto string
	fmt.Printf("Ingrese número de tarjeta (16 dígitos): ")
	fmt.Scanf("%s", &_nrotarjeta)
	fmt.Printf("Ingrese código de seguridad (4 dígitos): ")
	fmt.Scanf("%s", &codseguridad)
	fmt.Printf("Ingrese número de comercio: ")
	fmt.Scanf("%s", &nrocomercio)
	fmt.Printf("Ingrese monto: ")
	fmt.Scanf("%s", &monto)

	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query( `select autorizacioncompra($1, $2 ,$3, $4 );`,_nrotarjeta, codseguridad ,nrocomercio, monto )

	if err != nil {
		log.Fatal(err)
	}
}

func opcion_i() {
	fmt.Printf("Elegiste opcion i \n")

	var _nrocliente string
	var _año string
	var _mes string
	fmt.Printf("Ingrese número de cliente: ")
	fmt.Scanf("%s", &_nrocliente)
	fmt.Printf("Ingrese año del período: ")
	fmt.Scanf("%s", &_año)
	fmt.Printf("Ingrese mes del período: ")
	fmt.Scanf("%s", &_mes)

	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Query( `select generarResumen($1, $2 ,$3 );`,_nrocliente, _año, _mes)

	if err != nil {
		log.Fatal(err)
	}
}

func opcion_j() {
	fmt.Printf("Elegiste opcion j \n")

	db, err := bolt.Open("nosqldb", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
}

func opcion_k() {
	var clientes0 Cliente
	var clientes1 Cliente
	var clientes2 Cliente
	var tarjetas0 Tarjeta
	var tarjetas1 Tarjeta
	var tarjetas2 Tarjeta
	var comercios0 Comercio
	var comercios1 Comercio
	var comercios2 Comercio
	var compras0 Compra
	var compras1 Compra
	var compras2 Compra

	dbb, err := bolt.Open("nosqldb", 0600, nil)
	if err != nil {
		log.Fatal(err)
  	}
	defer dbb.Close()

	db,err := sql.Open("postgres", "user=postgres host=localhost dbname=facturacion sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	row := db.QueryRow( `select * from cliente where nrocliente=1;`)
	err = row.Scan(&clientes0.Nrocliente,&clientes0.Nombre,&clientes0.Apellido,&clientes0.Domicilio,&clientes0.Telefono)
	data, err := json.Marshal(clientes0)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "cliente", []byte(strconv.Itoa(clientes0.Nrocliente)), data)

	row = db.QueryRow( `select * from cliente where nrocliente=2;`)
	err = row.Scan(&clientes1.Nrocliente,&clientes1.Nombre,&clientes1.Apellido,&clientes1.Domicilio,&clientes1.Telefono)
	data, err = json.Marshal(clientes1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "cliente", []byte(strconv.Itoa(clientes1.Nrocliente)), data)

	row = db.QueryRow( `select * from cliente where nrocliente=3;`)
	err = row.Scan(&clientes2.Nrocliente,&clientes2.Nombre,&clientes2.Apellido,&clientes2.Domicilio,&clientes2.Telefono)
	data, err = json.Marshal(clientes2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "cliente", []byte(strconv.Itoa(clientes2.Nrocliente)), data)

	row = db.QueryRow( `select * from tarjeta where nrotarjeta='9746521427739620';`)
	err = row.Scan(&tarjetas0.Nrotarjeta,&tarjetas0.Nrocliente,&tarjetas0.Validadesde,&tarjetas0.Validahasta,&tarjetas0.Codseguridad,&tarjetas0.Limitecompra,&tarjetas0.Estado)
	data, err = json.Marshal(tarjetas0)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "tarjeta", []byte(tarjetas0.Nrotarjeta), data)

	row = db.QueryRow( `select * from tarjeta where nrotarjeta='3608343552173256';`)
	err = row.Scan(&tarjetas1.Nrotarjeta,&tarjetas1.Nrocliente,&tarjetas1.Validadesde,&tarjetas1.Validahasta,&tarjetas1.Codseguridad,&tarjetas1.Limitecompra,&tarjetas1.Estado)
	data, err = json.Marshal(tarjetas1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "tarjeta", []byte(tarjetas1.Nrotarjeta), data)

	row = db.QueryRow( `select * from tarjeta where nrotarjeta='2507045591093716';`)
	err = row.Scan(&tarjetas2.Nrotarjeta,&tarjetas2.Nrocliente,&tarjetas2.Validadesde,&tarjetas2.Validahasta,&tarjetas2.Codseguridad,&tarjetas2.Limitecompra,&tarjetas2.Estado)
	data, err = json.Marshal(tarjetas2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "tarjeta", []byte(tarjetas2.Nrotarjeta), data)

	row = db.QueryRow( `select * from comercio where nrocomercio=1;`)
	err = row.Scan(&comercios0.Nrocomercio,&comercios0.Nombre,&comercios0.Domicilio,&comercios0.Codigopostal,&comercios0.Telefono)
	data, err = json.Marshal(comercios0)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "comercio", []byte(strconv.Itoa(comercios0.Nrocomercio)), data)

	row = db.QueryRow( `select * from comercio where nrocomercio=2;`)
	err = row.Scan(&comercios1.Nrocomercio,&comercios1.Nombre,&comercios1.Domicilio,&comercios1.Codigopostal,&comercios1.Telefono)
	data, err = json.Marshal(comercios1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "comercio", []byte(strconv.Itoa(comercios1.Nrocomercio)), data)	

	row = db.QueryRow( `select * from comercio where nrocomercio=3;`)
	err = row.Scan(&comercios2.Nrocomercio,&comercios2.Nombre,&comercios2.Domicilio,&comercios2.Codigopostal,&comercios2.Telefono)
	data, err = json.Marshal(comercios2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "comercio", []byte(strconv.Itoa(comercios2.Nrocomercio)), data)	

	row = db.QueryRow( `select * from compra where nrooperacion=1;`)
	err = row.Scan(&compras0.Nrooperacion,&compras0.Nrotarjeta,&compras0.Nrocomercio,&compras0.Fecha,&compras0.Monto,&compras0.Pagado)
	data, err = json.Marshal(compras0)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "compra", []byte(strconv.Itoa(compras0.Nrooperacion)), data)

	row = db.QueryRow( `select * from compra where nrooperacion=2;`)	
	err = row.Scan(&compras1.Nrooperacion,&compras1.Nrotarjeta,&compras1.Nrocomercio,&compras1.Fecha,&compras1.Monto,&compras1.Pagado)
	data, err = json.Marshal(compras1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "compra", []byte(strconv.Itoa(compras1.Nrooperacion)), data)

	row = db.QueryRow( `select * from compra where nrooperacion=3;`)
	err = row.Scan(&compras2.Nrooperacion,&compras2.Nrotarjeta,&compras2.Nrocomercio,&compras2.Fecha,&compras2.Monto,&compras2.Pagado)
	data, err = json.Marshal(compras2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(dbb, "compra", []byte(strconv.Itoa(compras2.Nrooperacion)), data)
	
	if err != nil {
		log.Fatal(err)
	}
		
	resultado, err := ReadUnique(dbb, "cliente", []byte(strconv.Itoa(clientes0.Nrocliente)))
	fmt.Printf("%s\n", resultado)
	resultado, err = ReadUnique(dbb, "cliente", []byte(strconv.Itoa(clientes1.Nrocliente)))
	fmt.Printf("%s\n", resultado)
	resultado, err = ReadUnique(dbb, "cliente", []byte(strconv.Itoa(clientes2.Nrocliente)))
	fmt.Printf("%s\n", resultado)
	
	resultado, err = ReadUnique(dbb, "tarjeta", []byte(tarjetas0.Nrotarjeta))
	fmt.Printf("%s\n", resultado)
	resultado, err = ReadUnique(dbb, "tarjeta", []byte(tarjetas1.Nrotarjeta))
	fmt.Printf("%s\n", resultado)
	resultado, err = ReadUnique(dbb, "tarjeta", []byte(tarjetas2.Nrotarjeta))
	fmt.Printf("%s\n", resultado)
	
	resultado, err = ReadUnique(dbb, "comercio", []byte(strconv.Itoa(comercios0.Nrocomercio)))
	fmt.Printf("%s\n", resultado)
	resultado, err = ReadUnique(dbb, "comercio", []byte(strconv.Itoa(comercios1.Nrocomercio)))
	fmt.Printf("%s\n", resultado)
	resultado, err = ReadUnique(dbb, "comercio", []byte(strconv.Itoa(comercios2.Nrocomercio)))
	fmt.Printf("%s\n", resultado)
	
	resultado, err = ReadUnique(dbb, "compra", []byte(strconv.Itoa(compras0.Nrooperacion)))
	fmt.Printf("%s\n", resultado)	
	resultado, err = ReadUnique(dbb, "compra", []byte(strconv.Itoa(compras1.Nrooperacion)))
	fmt.Printf("%s\n", resultado)
	resultado, err = ReadUnique(dbb, "compra", []byte(strconv.Itoa(compras2.Nrooperacion)))
	fmt.Printf("%s\n", resultado)
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {

	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {

	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}
